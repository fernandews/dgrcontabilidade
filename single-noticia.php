<?php
// Template Name: Single Notícia
?>
<?php get_header(); ?>

<section class="body-post">
	<div class="content">
		<img src="<?php the_field('imagem_da_noticia'); ?>" alt="<?php the_field('titulo_da_noticia'); ?>">
		<p class="post-date"><?php the_field('data_da_noticia'); ?></p>
		<p><?php the_field('texto_da_noticia'); ?></p>
	</div>
</section>

<?php get_footer(); ?>