</main>
    <footer>
        <div class="container">
            <div class="footer">
                <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.1953754021947!2d-43.1354492859094!3d-22.906163485012115!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9983d604b5a7cb%3A0xee70747ae2049c4!2sINSTITUTO+DE+COMPUTA%C3%87%C3%83O!5e0!3m2!1spt-BR!2sbr!4v1564964886914!5m2!1spt-BR!2sbr" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="infos">
                    <div class="info-item">Rua Maestro Felício Tolêdo, 519 <br> Sala 502 - Centro de Niterói</div>
                    <div class="info-item">contato@dgrcontabilidade.com.br</div>
                    <div class="info-item">(21) 3619-5120 <br> (21) 3619-5120</div>
                </div>
                <div class="development">
                    <p>Desenvolvido por:</p>
                    <div class="logo-in"></div>
                </div>
            </div>
        </div>
        <p class="copyrights">Copyright Ⓒ 2013 - DGR Contabilidade todos os direitos reservados</p>
    </footer>

    <!-- Dizer que é um Footer -->
    <?php wp_footer(); ?>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script>new WOW().init();</script>
</html>