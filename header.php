<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DGR Contabilidade</title>

    <!-- Fontes e Arquivos CSS -->
    <link href="https://fonts.googleapis.com/css?family=Aldrich&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Serif&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/reset.css">
    <link rel="stylesheet" href="style.css">

    <!-- Dizer que é um Header -->
    <?php wp_head(); ?>
</head>
<body>
    <header>
        <div class="container">
            <p class="logo">DGR</p>
            <nav>
                <a href="">Serviços</a>
                <a href="">Notícias</a>
                <a href="">Contato</a>
                <div class="telephone">
                    <p class="tel-number">(99) 99999-9999</p><br>
                    <p class="tel-number">(99) 99999-9999</p>
                </div>
            </nav>
        </div>
    </header>
    <main>