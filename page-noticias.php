<?php
// Template Name: Notícias
?>
<?php get_header(); ?>
<section class="header-news">
    <div class="content">
        <div class="news-txt">
            <h2>Últimas Notícias</h2>
            <p>Fique por dentro de tudo!</p>
        </div>  
    </div>
</section>
<section class="newsletter">
    <div class="content">
        <div class="posts">
            <?php 
                $noticia = new WP_Query(
                    array(
                        'posts_per_page'   => 3,
                        'post_type'        => 'noticia',
                        'post_status'      => 'publish',
                        'suppress_filters' => true,
                        'orderby'          => 'post_date',
                        'order'            => 'DESC',
                        'paged'            => max(1 , get_query_var('paged'))
                    )
                );
            ?>
            <?php if($noticia -> have_posts()):
                while($noticia -> have_posts()): $noticia -> the_post(); ?>
                    <div class="posts-item wow fadeInUp" data-wow-delay=".5s">
                        <a href="<?php the_permalink(); ?>">
                            <img src="<?php the_field('imagem_da_noticia'); ?>" alt="<?php the_field('titulo_da_noticia'); ?>">
                            <h4><?php the_field('titulo_da_noticia'); ?></h4>
                            <p class="post-date"><?php the_field('data_da_noticia'); ?></p>
                            <p><?php the_field('resumo_da_noticia'); ?></p>
                        </a>
                    </div>
                <?php endwhile; ?>
                <div class="paginate">
                    <?php
                        //chamar a paginacao
                        $total_pages = $noticia->max_num_pages;
                        if ($total_pages > 1){
                            $current_page = max(1, get_query_var('paged'));
                                echo paginate_links(array(
                                    'base' 		=> get_pagenum_link(1) . '%_%',
                                    'format' 	=> '/page/%#%',
                                    'current' 	=> $current_page,
                                    'total' 	=> $total_pages,
                                    'prev_next' 	=> true,
                                    'prev_text' 	=> __('<'),
                                    'next_text' 	=> __('>')
                                ));
                            } 
                        ?>
                    </div>
                    <?php else: ?>
                        <p>Nenhuma notícia disponível</p>
            <?php endif; ?>
            <?php wp_reset_postdata(); ?>
        </div>    
        <div class="paginate">
            <a href="#" class="active"><p>1</p></a>
            <a href="#"><p>2</p></a>
            <a href="#"><p>3</p></a>
        </div>
    </div>
</section>
<section class="usefull-links">
    <div class= "content">
        <h5>Links Úteis</h5>
        <div class="links">
            <div class="link-item">
                <a href="#">Link 1</a>
                <p>Lorem ipsum sit dolor amet</p>
            </div>
            <div class="link-item">
                <a href="#">Link 1</a>
                <p>Lorem ipsum sit dolor amet</p>
            </div>
            <div class="link-item">
                <a href="#">Link 1</a>
                <p>Lorem ipsum sit dolor amet</p>
            </div>
            <div class="link-item">
                <a href="#">Link 1</a>
                <p>Lorem ipsum sit dolor amet</p>
            </div>
            <div class="link-item">
                <a href="#">Link 1</a>
                <p>Lorem ipsum sit dolor amet</p>
            </div>
            <div class="link-item">
                <a href="#">Link 1</a>
                <p>Lorem ipsum sit dolor amet</p>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>