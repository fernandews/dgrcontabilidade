<?php
// Template Name: Serviços
$home = get_page_by_title('Home');
?>

<?php get_header(); ?>
<section class="our-services">
    <div class="content">
        <h2><?php the_field('titulo_servicos');?></h2>
        <p><?php the_field('texto_servicos', $home);?></p>
        <div class="services">
            <div class="service-card">
                <p class="category"><?php the_field('titulo_box_1');?></p>
                <ul class="list">
                    <li><?php the_field('texto_box_1_1');?></li>
                    <li><?php the_field('texto_box_1_2');?></li>
                    <li><?php the_field('texto_box_1_3');?></li>
                    <li><?php the_field('texto_box_1_4');?></li>
                    <li><?php the_field('texto_box_1_5');?></li>
                    <li><?php the_field('texto_box_1_6');?></li>
                    <li><?php the_field('texto_box_1_7');?></li>
                </ul>
            </div>
            <div class="service-card">    
                <p class="category"><?php the_field('titulo_box_2');?></p>
                <ul class="list">
                    <li><?php the_field('texto_box_2_1');?></li>
                    <li><?php the_field('texto_box_2_2');?></li>
                    <li><?php the_field('texto_box_2_3');?></li>
                    <li><?php the_field('texto_box_2_4');?></li>
                    <li><?php the_field('texto_box_2_5');?></li>
                    <li><?php the_field('texto_box_2_6');?></li>
                    <li><?php the_field('texto_box_2_7');?></li>
                </ul>
            </div>
            <div class="service-card">
                <p class="category"><?php the_field('titulo_box_3');?></p>
                <ul class="list">
                    <li><?php the_field('texto_box_3_1');?></li>
                    <li><?php the_field('texto_box_3_2');?></li>
                    <li><?php the_field('texto_box_3_3');?></li>
                    <li><?php the_field('texto_box_3_4');?></li>
                    <li><?php the_field('texto_box_3_5');?></li>
                    <li><?php the_field('texto_box_3_6');?></li>
                    <li><?php the_field('texto_box_3_7');?></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
